#ifndef FILEWRITER_H
#define FILEWRITER_H

#include <QString>

#define NDIGITS 16

/* Sizes to suit your machine */
#define MAX_DIGIT 0xffffffff
#define MAX_HALF_DIGIT 0xffff
#define BITS_PER_DIGIT 32
#define HIBITMASK 0x80000000
#define B (MAX_HALF_DIGIT + 1)

/* Max number of digits expected in a 'big' array */
#define MAX_DIG_LEN 51

/* Useful macros */
#define LOHALF(x) ((DIGIT_T)((x) & 0xffff))
#define HIHALF(x) ((DIGIT_T)((x) >> 16 & 0xffff))
#define TOHIGH(x) ((DIGIT_T)((x) << 16))

#define ISODD(x) ((x) & 0x1)
#define ISEVEN(x) (!ISODD(x))

#define NEXTBITMASK( m, n ) if( m==1 ){m=HIBITMASK; n--;}else{m>>=1;}

typedef long unsigned uint32;
typedef uint32 DIGIT_T;
typedef struct
{
   // decryption key
   DIGIT_T d[NDIGITS];
   DIGIT_T n[NDIGITS];

   // encryption key
   DIGIT_T p[NDIGITS];
   DIGIT_T q[NDIGITS];
   DIGIT_T dp[NDIGITS];
   DIGIT_T dq[NDIGITS];
   DIGIT_T qinv[NDIGITS];
} RSA_KEY_DATA;

class FileWriter : public QObject
{
   public:
      static bool writeEncryptedData(const QString &outputFileName,
         const char *inputPrnString, unsigned code);

   private:
      static RSA_KEY_DATA rsa_key_data;

      FileWriter();
      ~FileWriter();

      static void decrypt(uint32 *data);
      static void encrypt(uint32 *data);
      static void init_crypt(void);

      static bool big_mod_exp(DIGIT_T y[], DIGIT_T x[], DIGIT_T e[],
         DIGIT_T m[], unsigned long ndigits);
      static unsigned int big_sizeof(DIGIT_T a[], unsigned long ndigits);
      static void
         big_set_equal(DIGIT_T a[], DIGIT_T b[], unsigned long ndigits);
      static void big_mod_mult(DIGIT_T a[], DIGIT_T x[], DIGIT_T y[],
         DIGIT_T m[], unsigned long ndigits);
      static void big_set_zero(DIGIT_T a[], unsigned long ndigits);
      static void big_multiply(DIGIT_T w[], DIGIT_T u[], DIGIT_T v[],
         unsigned long ndigits);
      static void single_multiply(DIGIT_T p[2], DIGIT_T x, DIGIT_T y);
      static void big_modulo(DIGIT_T r[], DIGIT_T u[], unsigned long udigits,
         DIGIT_T v[], unsigned long vdigits);
      static void big_divide(DIGIT_T q[], DIGIT_T r[], DIGIT_T u[],
         unsigned long udigits, DIGIT_T v[], unsigned long vdigits);
      static DIGIT_T big_mult_sub(DIGIT_T wn, DIGIT_T w[], DIGIT_T v[],
         DIGIT_T q, unsigned long n);
      static bool qhat_too_big(DIGIT_T qhat, DIGIT_T rhat, DIGIT_T vn2,
         DIGIT_T ujn2);
      static DIGIT_T big_short_div(DIGIT_T q[], DIGIT_T u[], DIGIT_T v,
         unsigned long ndigits);
      static int big_compare(DIGIT_T a[], DIGIT_T b[], unsigned long ndigits);
      static void big_set_digit(DIGIT_T a[], DIGIT_T d, unsigned long ndigits);
      static DIGIT_T big_shift_left(DIGIT_T a[], DIGIT_T b[], unsigned long x,
         unsigned long ndigits);
      static DIGIT_T single_divide(DIGIT_T *q, DIGIT_T *r, DIGIT_T u[2],
         DIGIT_T v);
      static void single_mult_sub(DIGIT_T uu[2], DIGIT_T qhat, DIGIT_T v1,
         DIGIT_T v0);
      static DIGIT_T big_add(DIGIT_T w[], DIGIT_T u[], DIGIT_T v[],
         unsigned long ndigits);
      static DIGIT_T big_sub(DIGIT_T w[], const DIGIT_T u[], const DIGIT_T v[],
         unsigned long ndigits);
      static DIGIT_T big_shift_right(DIGIT_T a[], DIGIT_T b[], unsigned long x,
         unsigned long ndigits);
      static int big_mod_inv(DIGIT_T inv[], DIGIT_T u[], DIGIT_T v[],
         unsigned long ndigits);
      static int big_is_zero(const DIGIT_T a[], unsigned long ndigits);
      static int short_cmp(const DIGIT_T a[], DIGIT_T b, unsigned long ndigits);
};

#endif
