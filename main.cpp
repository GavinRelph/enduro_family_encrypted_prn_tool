#include <QCoreApplication>
#include "filewriter.h"
#include <QFile>
#include <QDateTime>
#include <QTextStream>
#include <QDir>

int main(int argc, char *argv[])
{
    //QCoreApplication a(argc, argv);
    QString saveFileName;
    char* Command;
    QString Accesscode;
    QString LogLine;

    for (int i=0; i< argc; i++) {
        QString argCom = (argv[i]);
        if (argCom == "-a") {
            i++;
            Accesscode = argv[i];
        }
        else if (argCom == "-f") {
            i++;
            saveFileName = argv[i];
        }
        else if (argCom == "-c") {
            i++;
            Command = argv[i];
        };
    };

    while(true)
    {
        if (argc!=7){
            LogLine = "PRN Creation FAILURE = INCORRECT NUMBER OF ARGUMENTS" ;
            break;
        }

        if (Accesscode.length()!=8){
            LogLine = "PRN Creation FAILURE = INCORRECT ACCESSCODE LENGTH";
            break;
        }

        if ( !QString(Command).contains("REGPROJECT") ){
            if ( !QString(Command).contains("TAG11") ){
                if( !QString(Command).contains("TAG5") ){
                    LogLine = "PRN Creation FAILURE = ILLIGAL/INCORRECT COMMAND";
                    break;
                }
            }
        }

        bool success = FileWriter::writeEncryptedData(saveFileName, Command,
           Accesscode.toUInt(0, 16));

        if (success){
            LogLine = "PRN Creation success";
        }
        else {
            LogLine = "PRN Creation FAILURE = FAILED TO CREATE PRN FILE";
        };

        break;

    }

    QDir dir;

    QString Logfilename = dir.absolutePath() + "\\EPRNLog.log";
    QFile file(Logfilename);
    QString time = QDateTime::currentDateTime().toTimeSpec(Qt::OffsetFromUTC).toString(Qt::ISODate);;
    file.open(QIODevice::Append);
    QTextStream stream(&file);
    stream << time + " | " +  QString(Command) + " | " + saveFileName + " | " + LogLine << endl;
    file.close();
    return 0;
}
