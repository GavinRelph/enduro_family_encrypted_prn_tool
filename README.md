**Command line driven encrypted PRN tool**

This tool was created from the standard GUI tool to allow it to be called via PHP on web based applications. 

The tool has limited functionality only allowing for the creation of PRN's containing 'TAG5', 'TAG11' and 'REGPROJECT' commands. This can be removed in the source if required but the tool was initially created for use with project coding of film and printers.

The command line arguments are structured as follows:

	EPRN.exe -a [access code] -c [command] -f [outputfile]

		access code - The code given by printer or device in the query file

		command � The command to be placed in the PRN this is currently restricted to 'TAG5', 'TAG11' and 'REGPROJECT' commands.

		Outputfile � the absolute path, including file name, of the output PRN


A log is created of each input receved. The time, command, output file and error/success are all recorded. This log is updated in the working directory of the exe.