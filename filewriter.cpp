#include <cstring>

#include <QString>
#include <QFile>
//#include <QMessageBox>

#include "filewriter.h"

RSA_KEY_DATA FileWriter::rsa_key_data;


bool FileWriter::writeEncryptedData(const QString &outputFileName,
                                    const char *inputPrnString,
                                    unsigned code)
{
   static char prefix[] = {
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x01, 'R', 'E', 'Q', ',', 'E', 'C', 'S', '2', '4', '9', ',', 0x1c,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
      0x1c
   };

   static char suffix[] = {
      0x03
   };

   char input_buffer[64];

   // open input file

   if(!inputPrnString) {
      return false;
   }

   // validate size

   int inputSize = std::strlen(inputPrnString);
   //if(inputSize > 59) {
      //int ans = QMessageBox::warning(NULL, "Warning",
          //QString("The selected input file "
            //"is greater than 59 bytes in size,\n"
            //"and only the first 59 bytes will be read.\n"
            //"Do you wish to continue?"),
         //"&Yes", "&No", QString::null, 1, 1);
      //if(ans == 1) {
         //return false;
     // }

      //inputSize = 59;
   //}

   std::memcpy(input_buffer + 4, inputPrnString, inputSize);

   // add terminating null byte

   input_buffer[inputSize + 4] = '\0';

  // Switch endianness of buffer contents prior to encoding.

   for(unsigned i = 0; i < sizeof(input_buffer); i += 4) {
      char tmp = input_buffer[i];
      input_buffer[i] = input_buffer[i + 3];
      input_buffer[i + 3] = tmp;
      tmp = input_buffer[i + 1];
      input_buffer[i + 1] = input_buffer[i + 2];
      input_buffer[i + 2] = tmp;
   }

   // Insert access code (little-endian order).

   input_buffer[0] = (char)code;
   input_buffer[1] = (char)(code >> 8);
   input_buffer[2] = (char)(code >> 16);
   input_buffer[3] = (char)(code >> 24);

   // Encrypt buffer contents.

   init_crypt();
   encrypt((uint32 *)input_buffer);

   // Switch endianness of encrypted buffer contents.

   for(unsigned i = 0; i < sizeof(input_buffer); i += 4) {
      char tmp = input_buffer[i];
      input_buffer[i] = input_buffer[i + 3];
      input_buffer[i + 3] = tmp;
      tmp = input_buffer[i + 1];
      input_buffer[i + 1] = input_buffer[i + 2];
      input_buffer[i + 2] = tmp;
   }

   // Write data to output file.
   QFile outputFile(outputFileName);
   if(!outputFile.open(QIODevice::WriteOnly)) {
      return false;
   }
   outputFile.write(prefix, sizeof(prefix));
   outputFile.write(input_buffer, sizeof(input_buffer));
   outputFile.write(suffix, sizeof(suffix));
   outputFile.close();
   return true;
}

/***********************************************************************

                           decrypt()

  Decrypt a 512-bit block of data in place, using straightforward
  exponentiation.
  
  Input parameters: data - pointer to the data to decrypt
  Returns:          void

 ***********************************************************************/

void FileWriter::decrypt(uint32 *data)
{
   DIGIT_T temp[NDIGITS];

   big_mod_exp( temp, data,
      rsa_key_data.d, rsa_key_data.n, NDIGITS );

   memcpy(data, temp, NDIGITS * 4);
}

/***********************************************************************

                           encrypt()

  Encrypt a 512-bit block of data in place, using CRT method (for speed).

  Input parameters: data - pointer to the data to encrypt
  Returns:          void

 ***********************************************************************/

void FileWriter::encrypt(uint32 *data)
{
   DIGIT_T m1[NDIGITS], m2[NDIGITS], h[NDIGITS], temp[NDIGITS];

   // m1 = data^dp mod p ; m2 = data^dq mod q
   big_mod_exp(m1, data, rsa_key_data.dp, rsa_key_data.p, NDIGITS);
   big_mod_exp(m2, data, rsa_key_data.dq, rsa_key_data.q, NDIGITS);

   // ensure m1 >= m2
   if(big_compare(m1, m2, NDIGITS) < 0) {
      big_add(temp, m1, rsa_key_data.p, NDIGITS);
      memcpy(m1, temp, NDIGITS*4);
   }

   // h = qinv*(m1 - m2) mod p
   big_sub(temp, m1, m2, NDIGITS);
   big_mod_mult(h, rsa_key_data.qinv, temp, rsa_key_data.p, NDIGITS);

   // message = m2 + h*q
   big_multiply(temp, h, rsa_key_data.q, NDIGITS/2);
   big_add(data, m2, temp, NDIGITS);
}

/***********************************************************************

                           init_crypt()

  Initialises the crypt module.  Must be called prior to any encryption
  or decryption functions.
  
  Input parameters: void
  Returns:          void

 ***********************************************************************/

void FileWriter::init_crypt(void)
{
   memset(&rsa_key_data, '\0', sizeof(RSA_KEY_DATA));

   // decryption exponent
   rsa_key_data.d[0] = 0x00010001;

   // modulus
   rsa_key_data.n[0] = 0x1e618763;
   rsa_key_data.n[1] = 0x67632979;
   rsa_key_data.n[2] = 0xca48c2bd;
   rsa_key_data.n[3] = 0x35d6f338;
   rsa_key_data.n[4] = 0x3fac125f;
   rsa_key_data.n[5] = 0x6c0f69ac;
   rsa_key_data.n[6] = 0x076b0998;
   rsa_key_data.n[7] = 0x8935e564;
   rsa_key_data.n[8] = 0x292b7d4a;
   rsa_key_data.n[9] = 0x31ce5059;
   rsa_key_data.n[10] = 0xa1fe133e;
   rsa_key_data.n[11] = 0x60c8aed8;
   rsa_key_data.n[12] = 0x145402a6;
   rsa_key_data.n[13] = 0x29450755;
   rsa_key_data.n[14] = 0x4deb0382;
   rsa_key_data.n[15] = 0xf821aa54;

   // encryption key (p, q, dP, dQ, qInv)
   rsa_key_data.p[0] = 0xb8e30593;
   rsa_key_data.p[1] = 0x5c1d18a6;
   rsa_key_data.p[2] = 0x4b23d31f;
   rsa_key_data.p[3] = 0xef6c371b;
   rsa_key_data.p[4] = 0xe7ecce4c;
   rsa_key_data.p[5] = 0x6476e8d6;
   rsa_key_data.p[6] = 0x973b9b50;
   rsa_key_data.p[7] = 0xfd63a002;
   rsa_key_data.q[0] = 0xbd7e98f1;
   rsa_key_data.q[1] = 0x91c2230c;
   rsa_key_data.q[2] = 0xa29072cd;
   rsa_key_data.q[3] = 0x6e9f2fc7;
   rsa_key_data.q[4] = 0x2b4d5f87;
   rsa_key_data.q[5] = 0x82e68b72;
   rsa_key_data.q[6] = 0xb6790b11;
   rsa_key_data.q[7] = 0xfab02c06;
   rsa_key_data.dp[0] = 0x88477daf;
   rsa_key_data.dp[1] = 0xdc1743ef;
   rsa_key_data.dp[2] = 0x60db326c;
   rsa_key_data.dp[3] = 0xed462804;
   rsa_key_data.dp[4] = 0x3a11b9be;
   rsa_key_data.dp[5] = 0x8342c9f7;
   rsa_key_data.dp[6] = 0x77338d25;
   rsa_key_data.dp[7] = 0xe2493919;
   rsa_key_data.dq[0] = 0x6aaa1e11;
   rsa_key_data.dq[1] = 0xd61993be;
   rsa_key_data.dq[2] = 0xf0f0f090;
   rsa_key_data.dq[3] = 0x8089f130;
   rsa_key_data.dq[4] = 0x8952af7f;
   rsa_key_data.dq[5] = 0x7f49cea7;
   rsa_key_data.dq[6] = 0x805fe0ae;
   rsa_key_data.dq[7] = 0x9273ff94;
   rsa_key_data.qinv[0] = 0xe502df6c;
   rsa_key_data.qinv[1] = 0xa521d690;
   rsa_key_data.qinv[2] = 0x9c5acc41;
   rsa_key_data.qinv[3] = 0x4b2b7231;
   rsa_key_data.qinv[4] = 0xe9b11f59;
   rsa_key_data.qinv[5] = 0xda23deb4;
   rsa_key_data.qinv[6] = 0x5f14a540;
   rsa_key_data.qinv[7] = 0x70b4268d;
}

/*********************************************************************/

bool FileWriter::big_mod_exp( DIGIT_T y[], DIGIT_T x[], DIGIT_T e[], DIGIT_T m[], 
                 unsigned long ndigits )
/*
  Function computes y = x^e mod m for big numbers
  Uses binary left-to-right method
*/
{
  DIGIT_T mask;
  unsigned long n;
  
  if( ndigits == 0 )
    return false;

	/* Find second-most significant bit in e */
  n = big_sizeof( e, ndigits );
  for( mask = HIBITMASK; mask>0; mask>>=1 )
  {
    if( e[n-1] & mask )
      break;
  }
  NEXTBITMASK( mask, n );

  /* Set y = x */
  big_set_equal (y, x, ndigits );

	/* For bit j = k-2 downto 0 step -1 */
  while ( n )
	{
    big_mod_mult( y, y, y, m, ndigits );   // Square
    if( e[n-1] & mask )
      big_mod_mult( y, y, x, m, ndigits ); // Multiply
		
		/* Move to next bit */
    NEXTBITMASK( mask, n );
  }
  
  return true;
}

/*********************************************************************/

unsigned int FileWriter::big_sizeof( DIGIT_T a[], unsigned long ndigits )
/*
  Function returns size of significant digits in a
*/
{
  while ( ndigits-- )
  {
    if( a[ndigits] != 0 )
      return ( ++ndigits );
  }
  return ( 0 );
}

/*********************************************************************/

void FileWriter::big_set_equal( DIGIT_T a[], DIGIT_T b[], unsigned long ndigits )
/*
  Function sets a = b
*/
{
	unsigned long i;
	
	for( i=0; i<ndigits; i++ )
	{
		a[i] = b[i];
	}
}

/*********************************************************************/

void FileWriter::big_mod_mult( DIGIT_T a[], DIGIT_T x[], DIGIT_T y[], 
                          DIGIT_T m[], unsigned long ndigits )
/*
  Function computes a = (x * y) mod m
*/
{	
  /* Double-length temp variable */
  DIGIT_T p[MAX_DIG_LEN*2];

	big_set_zero( p, ndigits*2 );

  /* Calc p[2n] = x * y */
  big_multiply( p, x, y, ndigits );

  /* Then modulo */
  big_modulo( a, p, ndigits*2, m, ndigits );
}

/*********************************************************************/

void FileWriter::big_set_zero( DIGIT_T a[], unsigned long ndigits )
/*
  Function sets a = 0
*/
{
  unsigned long i;
	
  for( i=0; i<ndigits; i++ )
  {
    a[i] = 0;
  }
}

/*********************************************************************/

void FileWriter::big_multiply( DIGIT_T w[], DIGIT_T u[], DIGIT_T v[], 
                          unsigned long ndigits )
/*
  Function computes product w = u * v
  Where u and v are multiprecision integers of ndigits each
  and w is a multiprecision integer of 2*ndigits

  Ref: Knuth Vol 2 Ch 4.3.1 p 268 Algorithm M
*/
{
  DIGIT_T k, t[2];
  unsigned long i, j, m, n;

  m = n = ndigits;

  /* Step M1. Initialise */
  for( i=0; i<2*m; i++ )
    w[i] = 0;

  for( j=0; j<n; j++ )
  {
    /* Step M2. Zero multiplier? */
    if( v[j] == 0 )
    {
      w[j + m] = 0;
    }
    else
    {
      /* Step M3. Initialise i */
      k = 0;
      for( i=0; i<m; i++ )
      {
        /* Step M4. Multiply and add */
        /* t = u_i * v_j + w_(i+j) + k */
        single_multiply( t, u[i], v[j] );

        t[0] += k;
        if( t[0] < k )
          t[1]++;
        t[0] += w[i+j];
        if( t[0] < w[i+j] )
          t[1]++;

        w[i+j] = t[0];
        k = t[1];
      }	
      /* Step M5. Loop on i, set w_(j+m) = k */
      w[j+m] = k;
    }
  }	/* Step M6. Loop on j */
}

/*********************************************************************/

void FileWriter::single_multiply( DIGIT_T p[2], DIGIT_T x, DIGIT_T y)
/*
  Function computes p = x * y
  Ref: Arbitrary Precision Computation
  http://numbers.computation.free.fr/Constants/constants.html
  
         high    p1                p0     low
        +--------+--------+--------+--------+
        |      x1*y1      |      x0*y0      |
        +--------+--------+--------+--------+
               +-+--------+--------+
               |1| (x0*y1 + x1*y1) |
               +-+--------+--------+
                ^carry from adding (x0*y1+x1*y1) together
                        +-+
                        |1|< carry from adding LOHALF t
                        +-+  to high half of p0
*/
{
  DIGIT_T x0, y0, x1, y1;
  DIGIT_T t, u, carry;

	/*  Split each x,y into two halves
      x = x0 + B*x1
      y = y0 + B*y1
      where B = 2^16, half the digit size
      Product is xy = x0y0 + B(x0y1 + x1y0) + B^2(x1y1)
	*/

  x0 = LOHALF( x );
  x1 = HIHALF( x );
  y0 = LOHALF( y );
  y1 = HIHALF( y );
	
  /* Calc low part - no carry */
  p[0] = x0 * y0;

  /* Calc middle part */
  t = x0 * y1;
  u = x1 * y0;
  t += u;
  if( t<u )
    carry = 1;
  else
    carry = 0;

  /*  This carry will go to high half of p[1]
      + high half of t into low half of p[1] */
  carry = TOHIGH( carry ) + HIHALF( t );

  /* Add low half of t to high half of p[0] */
  t = TOHIGH( t );
  p[0] += t;
  
  if( p[0]<t )
    carry++;

  p[1] = x1 * y1;
  p[1] += carry;
}

/*********************************************************************/

void FileWriter::big_modulo( DIGIT_T r[], DIGIT_T u[], unsigned long udigits, 
                        DIGIT_T v[], unsigned long vdigits )
/*
  Function calculates r = u mod v
  where r, v are multiprecision integers of length vdigits
  and u is a multiprecision integer of length udigits.
  r may overlap v.

  Note that r here is only vdigits long, 
  whereas in big_divide it is udigits long.

  Use remainder from big_divide function.
*/
{
  /* Double-length temp variable for divide fn */
  DIGIT_T qq[MAX_DIG_LEN * 2];
  /* Use a double-length temp for r to allow overlap of r and v */
  DIGIT_T rr[MAX_DIG_LEN * 2];

	big_set_zero( rr, udigits );
	big_set_zero( qq, udigits );
	
  /* rr[2n] = u[2n] mod v[n] */
	big_divide( qq, rr, u, udigits, v, vdigits );
	big_set_equal( r, rr, vdigits );
}

/*********************************************************************/

void FileWriter::big_divide( DIGIT_T q[], DIGIT_T r[], DIGIT_T u[],
                        unsigned long udigits, DIGIT_T v[],
                        unsigned long vdigits )
/*
  Function computes quotient q = u / v and remainder r = u mod v
  where q, r, u are multiple precision digits
  all of udigits and the divisor v is vdigits.

  Ref: Knuth Vol 2 Ch 4.3.1 p 272 Algorithm D.

  Do without extra storage space, i.e. use r[] for
  normalised u[], unnormalise v[] at end, and cope with
  extra digit Uj+n added to u after normalisation.

  WARNING: this trashes q and r first, so cannot do
  u = u / v or v = u mod v.
*/
{
  unsigned long shift;
  int n, m, j;
  DIGIT_T bitmask, overflow;
  DIGIT_T qhat, rhat, t[2];
  DIGIT_T *uu, *ww;
  int qhatOK, cmp;

  /* Clear q and r */
  big_set_zero( q, udigits );
  big_set_zero( r, udigits );

  /* Work out exact sizes of u and v */
  n = (int)big_sizeof( v, vdigits );
  m = (int)big_sizeof( u, udigits );
  m -= n;

	/* Catch special cases */
  if( n==0 )
		return; // Error: divide by zero

	if( n==1 )  // Use short division instead
  { 
    r[0] = big_short_div( q, u, v[0], udigits );
    return;
  }

  if( m<0 ) // v > u, so just set q = 0 and r = u
  { 
    big_set_equal( r, u, udigits );
    return;
  }

  if( m==0 ) // u and v are the same length
  {
    cmp = big_compare( u, v, (unsigned long)n );
    if( cmp<0 ) // v > u, as above 
    {
      big_set_equal( r, u, udigits );
      return;
    }
    else if( cmp==0 ) // v == u, so set q = 1 and r = 0
    {
      big_set_digit( q, 1, udigits );
      return;
    }
  }

  /*  In Knuth notation, we have:
      Given
      u = (Um+n-1 ... U1U0)
      v = (Vn-1 ... V1V0)
      Compute
      q = u/v = (QmQm-1 ... Q0)
      r = u mod v = (Rn-1 ... R1R0)
	*/

  /*  Step D1. Normalise */
  /*  Requires high bit of Vn-1
      to be set, so find most signif. bit then shift left,
      i.e. d = 2^shift, u' = u * d, v' = v * d.
  */
  bitmask = HIBITMASK;
  for( shift=0; shift<BITS_PER_DIGIT; shift++ )
  {
    if( v[n-1] & bitmask )
      break;
    bitmask >>= 1;
  }

  /* Normalise v in situ - NB only shift non-zero digits */
  overflow = big_shift_left( v, v, shift, n );

  /* Copy normalised dividend u*d into r */
  overflow = big_shift_left( r, u, shift, n+m );
  uu = r;  // Use ptr to keep notation constant

  t[0] = overflow;  // New digit Um+n

  /* Step D2. Initialise j. Set j = m */
  for( j=m; j>=0; j-- )
  {
    /* Step D3. Calculate Qhat = (b.Uj+n + Uj+n-1)/Vn-1 */
    qhatOK = 0;
    t[1] = t[0]; // This is Uj+n
    t[0] = uu[j+n-1];
    overflow = single_divide( &qhat, &rhat, t, v[n-1] );

    /* Test Qhat */
    if( overflow )
    { // Qhat = b
      qhat = MAX_DIGIT;
      rhat = uu[j+n-1];
      rhat += v[n-1];
      if( rhat<v[n-1] ) // Overflow
        qhatOK = 1;
    }
    if( !qhatOK && qhat_too_big( qhat, rhat, v[n-2], uu[j+n-2] ))
    { // Qhat.Vn-2 > b.Rhat + Uj+n-2
      qhat--;
      rhat += v[n-1];
      if( !(rhat < v[n-1] ))
        if (qhat_too_big( qhat, rhat, v[n-2], uu[j+n-2] ))
          qhat--;
    }

    /* Step D4. Multiply and subtract */
    ww = &uu[j];
    overflow = big_mult_sub( t[1], ww, v, qhat, (unsigned long)n );

    /* Step D5. Test remainder. Set Qj = Qhat */
    q[j] = qhat;
    if( overflow )
    { // Step D6. Add back if D4 was negative
      q[j]--;
      overflow = big_add( ww, ww, v, (unsigned long)n );
    }

    t[0] = uu[j+n-1];	// Uj+n on next round

  } // Step D7. Loop on j

  /* Clear high digits in uu */
  for( j=n; j<m+n; j++ )
    uu[j] = 0;

  /* Step D8. Unnormalise. */
  big_shift_right( r, r, shift, n );
  big_shift_right( v, v, shift, n );
}

/*********************************************************************/

DIGIT_T FileWriter::big_mult_sub( DIGIT_T wn, DIGIT_T w[], DIGIT_T v[],
                             DIGIT_T q, unsigned long n)
/*
  Function computes w = w - qv
  where w = (WnW[n-1]...W[0])
  
  Returns modified Wn
*/
{	
  DIGIT_T k, t[2];
  unsigned long i;

  if( q==0 )	// No change
    return ( wn );

  k = 0;

  for( i=0; i<n; i++ )
  {
    single_multiply( t, q, v[i] );
    w[i] -= k;
    if( w[i] > MAX_DIGIT-k )
      k=1;
    else
      k=0;
    w[i] -= t[0];
    if( w[i] > MAX_DIGIT-t[0] )
      k++;
    k += t[1];
  }

  /* Cope with Wn not stored in array w[0..n-1] */
  wn -= k;

  return ( wn );
}

/*********************************************************************/

bool FileWriter::qhat_too_big( DIGIT_T qhat, DIGIT_T rhat,
                          DIGIT_T vn2, DIGIT_T ujn2 )
/*
  Function returns true if Qhat is too big
		i.e. if (Qhat * Vn-2) > (b.Rhat + Uj+n-2)
*/
{
  DIGIT_T t[2];

  single_multiply( t, qhat, vn2 );
  if( t[1] < rhat )
    return false;
  else if( t[1] > rhat )
    return true;
  else if( t[0] > ujn2 )
    return true;

  return false;
}

/*********************************************************************/

DIGIT_T FileWriter::big_short_div( DIGIT_T q[], DIGIT_T u[], DIGIT_T v, 
				                      unsigned long ndigits )
/* 
  Function calculates quotient q = u div v
	Returns remainder r = u mod v
	where q, u are multiprecision integers of ndigits each
	and d, v are single precision digits.

	Makes no assumptions about normalisation.
		
	Ref: Knuth Vol 2 Ch 4.3.1 Exercise 16 p625
*/				                      
{
	unsigned long j;
	DIGIT_T t[2], r;
	unsigned int shift;
	DIGIT_T bitmask, overflow, *uu;

	if( ndigits==0 )
	  return ( 0 );
	if( v==0 )
		return ( 0 );	// Divide by zero error

	/*	Normalise first */
	/*	Requires high bit of V
		to be set, so find most signif. bit then shift left,
		i.e. d = 2^shift, u' = u * d, v' = v * d.
	*/
	bitmask = HIBITMASK;
	for( shift=0; shift<BITS_PER_DIGIT; shift++ )
	{
		if( v & bitmask )
			break;
		bitmask >>= 1;
	}

	v <<= shift;
	overflow = big_shift_left( q, u, shift, ndigits );
	uu = q;
	
	/* Step S1 - modified for extra digit. */
	r = overflow;	/* New digit Un */
	j = ndigits;
	while ( j-- )
	{
		/* Step S2. */
		t[1] = r;
		t[0] = uu[j];
		overflow = single_divide( &q[j], &r, t, v );
	}

  /* Unnormalise */
  r >>= shift;
	
  return ( r );
}

/*********************************************************************/

int FileWriter::big_compare( DIGIT_T a[], DIGIT_T b[], unsigned long ndigits)
/*
  Function returns sign of (a - b)
*/
{
  if( ndigits == 0)
    return ( 0 );

	while ( ndigits-- )
	{
		if( a[ndigits] > b[ndigits] )
			return ( 1 );	// GT
		if( a[ndigits] < b[ndigits] )
			return ( -1 );	// LT
	}
	return ( 0 );	// EQ
}

/*********************************************************************/

void FileWriter::big_set_digit( DIGIT_T a[], DIGIT_T d, unsigned long ndigits )
/*
  Function sets a = d where d is a single digit
*/
{
	unsigned long i;
	
	for( i=1; i<ndigits; i++ )
	{
		a[i] = 0;
	}
	a[0] = d;
}

/*********************************************************************/

DIGIT_T FileWriter::big_shift_left( DIGIT_T a[], DIGIT_T b[],
                               unsigned long x, unsigned long ndigits )
/*
  Function computes a = b << x
*/                               
{
	unsigned long i, y;
	DIGIT_T mask, carry, nextcarry;

	/* Check input - NB unspecified result */
	if( x >= BITS_PER_DIGIT )
		return ( 0 );

	/* Construct mask */
	mask = HIBITMASK;
	for( i=1; i<x; i++ )
	{
		mask = (mask >> 1) | mask;
	}
	if( x==0 )
	  mask = 0x0;
	
	y = BITS_PER_DIGIT - x;
	carry = 0;
	for( i=0; i<ndigits; i++ )
	{
		nextcarry = (b[i] & mask) >> y;
		a[i] = b[i] << x | carry;
		carry = nextcarry;
	}
	return ( carry );
}

/*********************************************************************/

DIGIT_T FileWriter::single_divide( DIGIT_T *q, DIGIT_T *r, DIGIT_T u[2], 
                              DIGIT_T v )
/*
  Function computes quotient q = u / v, remainder r = u mod v
	where u is a double digit
	and q, v, r are single precision digits.
	Returns high digit of quotient (max value is 1)
	Assumes normalised such that v1 >= b/2
	where b is size of HALF_DIGIT
	i.e. the most significant bit of v should be one

	In terms of half-digits in Knuth notation:
	(q2q1q0) = (u4u3u2u1u0) / (v1v0)
	(r1r0) = (u4u3u2u1u0) mod (v1v0)
	for m = 2, n = 2 where u4 = 0
	q2 is either 0 or 1.
	We set q = (q1q0) and return q2 as "overflow'
*/
{
	DIGIT_T qhat, rhat, t, v0, v1, u0, u1, u2, u3;
	DIGIT_T uu[2], q2;

	/* Check for normalisation */
	if(!(v & HIBITMASK ))
	{
		*q = *r = 0;
		return ( MAX_DIGIT );
	}
	
	/* Split up into half-digits */
	v0 = LOHALF(v);
	v1 = HIHALF(v);
	u0 = LOHALF(u[0]);
	u1 = HIHALF(u[0]);
	u2 = LOHALF(u[1]);
	u3 = HIHALF(u[1]);

	/* Do three rounds of Knuth Algorithm D Vol 2 p272 */

	/*	ROUND 1. Set j = 2 and calculate q2 */
	/*	Estimate qhat = (u4u3)/v1  = 0 or 1 
		then set (u4u3u2) -= qhat(v1v0)
		where u4 = 0.
	*/
	qhat = u3 / v1;
	if( qhat > 0 )
	{
		rhat = u3 - qhat * v1;
		t = TOHIGH( rhat ) | u2;
		if( qhat * v0 > t )
			qhat--;
	}
	uu[1] = 0;		// (u4)
	uu[0] = u[1];	// (u3u2)
	if( qhat > 0 )
	{
		/* (u4u3u2) -= qhat(v1v0) where u4 = 0 */
		single_mult_sub( uu, qhat, v1, v0 );
		if( HIHALF(uu[1]) != 0 )
		{	// Add back
			qhat--;
			uu[0] += v;
			uu[1] = 0;
		}
	}
	q2 = qhat;

	/*	ROUND 2. Set j = 1 and calculate q1 */
	/*	Estimate qhat = (u3u2) / v1 
		then set (u3u2u1) -= qhat(v1v0)
	*/
	t = uu[0];
	qhat = t / v1;
	rhat = t - qhat * v1;
	/* Test on v0 */
	t = TOHIGH( rhat ) | u1;
	if( (qhat == B) || (qhat * v0 > t))
	{
		qhat--;
		rhat += v1;
		t = TOHIGH( rhat ) | u1;
		if( (rhat < B) && (qhat * v0 > t))
			qhat--;
	}

	/*	Multiply and subtract 
		(u3u2u1)' = (u3u2u1) - qhat(v1v0)	
	*/
	uu[1] = HIHALF( uu[0] );	// (0u3)
	uu[0] = TOHIGH( LOHALF( uu[0])) | u1;	// (u2u1)
	single_mult_sub( uu, qhat, v1, v0 );
	if( HIHALF( uu[1] ) != 0 )
	{	// Add back
		qhat--;
		uu[0] += v;
		uu[1] = 0;
	}

	/* q1 = qhat */
	*q = TOHIGH( qhat );

	/* ROUND 3. Set j = 0 and calculate q0 */
	/*	Estimate qhat = (u2u1) / v1
		then set (u2u1u0) -= qhat(v1v0)
	*/
	t = uu[0];
	qhat = t / v1;
	rhat = t - qhat * v1;
	/* Test on v0 */
	t = TOHIGH( rhat ) | u0;
	if( (qhat == B) || (qhat * v0 > t))
	{
		qhat--;
		rhat += v1;
		t = TOHIGH( rhat ) | u0;
		if( ( rhat < B) && (qhat * v0 > t))
			qhat--;
	}

	/*	Multiply and subtract 
		(u2u1u0)" = (u2u1u0)' - qhat(v1v0)
	*/
	uu[1] = HIHALF( uu[0] );	// (0u2) 
	uu[0] = TOHIGH(LOHALF(uu[0])) | u0;	// (u1u0) 
	single_mult_sub(uu, qhat, v1, v0);
	if (HIHALF(uu[1]) != 0)
	{	// Add back
		qhat--;
		uu[0] += v;
		uu[1] = 0;
	}

	/* q0 = qhat */
	*q |= LOHALF( qhat );

	/* Remainder is in (u1u0) i.e. uu[0] */
	*r = uu[0];
	return q2;
}

/*********************************************************************/

void FileWriter::single_mult_sub( DIGIT_T uu[2], DIGIT_T qhat, 
					                   DIGIT_T v1, DIGIT_T v0 )
/*
  Function computes uu = uu - q(v1v0) 
	where uu = u3u2u1u0, u3 = 0
	and u_n, v_n are all half-digits
	even though v1, v2 are passed as full digits.
*/					                   
{
	DIGIT_T p0, p1, t;

	p0 = qhat * v0;
	p1 = qhat * v1;
	t = p0 + TOHIGH( LOHALF(p1) );
	uu[0] -= t;
	if( uu[0] > MAX_DIGIT - t )
		uu[1]--;	// Borrow
	uu[1] -= HIHALF( p1 );
}

/*********************************************************************/

DIGIT_T FileWriter::big_add( DIGIT_T w[], DIGIT_T u[], DIGIT_T v[], 
			                  unsigned long ndigits )
/*
  Function calculates w = u + v
	where w, u, v are multiprecision integers of ndigits each
	Returns carry if overflow. Carry = 0 or 1.

	Ref: Knuth Vol 2 Ch 4.3.1 p 266 Algorithm A.
*/			                  
{
	DIGIT_T k;
	unsigned long j;

	/* Step A1. Initialise */
	k = 0;

	for( j=0; j<ndigits; j++ )
	{
		/*	Step A2. Add digits w_j = (u_j + v_j + k)
			Set k = 1 if carry (overflow) occurs
		*/
		w[j] = u[j] + k;
		if( w[j] < k )
			k = 1;
		else
			k = 0;
		
		w[j] += v[j];
		if (w[j] < v[j])
			k++;

	}	/* Step A3. Loop on j */

	return ( k );	// w_n = k 
}

/*********************************************************************/

DIGIT_T FileWriter::big_sub(DIGIT_T w[], const DIGIT_T u[], const DIGIT_T v[], 
			    unsigned long ndigits)
{
	/*	Calculates w = u - v where u >= v
		w, u, v are multiprecision integers of ndigits each
		Returns 0 if OK, or 1 if v > u.

		Ref: Knuth Vol 2 Ch 4.3.1 p 267 Algorithm S.
	*/

	DIGIT_T k;
	unsigned long j;

	/* Step S1. Initialise */
	k = 0;

	for (j = 0; j < ndigits; j++)
	{
		/*	Step S2. Subtract digits w_j = (u_j - v_k - k)
			Set k = 1 if borrow occurs.
		*/
		w[j] = u[j] - k;
		if (w[j] > MAX_DIGIT - k)
			k = 1;
		else
			k = 0;
		
		w[j] -= v[j];
		if (w[j] > MAX_DIGIT - v[j])
			k++;

	}	/* Step S3. Loop on j */

	return k;	/* Should be zero if u >= v */
}

/*********************************************************************/

DIGIT_T FileWriter::big_shift_right( DIGIT_T a[], DIGIT_T b[],
                                unsigned long x, unsigned long ndigits )
/*
  Function computes a = b >> x 
*/                                
{
	unsigned long i, y;
	DIGIT_T mask, carry, nextcarry;

	/* Check input  - NB unspecified result */
	if( x >= BITS_PER_DIGIT )
		return ( 0 );

	/* Construct mask */
	mask = 0x1;
	for( i=1; i<x; i++ )
	{
		mask = (mask << 1) | mask;
	}
	if( x==0 )
	  mask = 0x0;
	
	y = BITS_PER_DIGIT - x;
	carry = 0;
	i = ndigits;
	
	while ( i-- )
	{
		nextcarry = (b[i] & mask) << y;
		a[i] = b[i] >> x | carry;
		carry = nextcarry;
  }
  return ( carry );
}

int FileWriter::big_mod_inv(DIGIT_T inv[], DIGIT_T u[], DIGIT_T v[], unsigned long ndigits)
{	/*	Computes inv = u^(-1) mod v */
	/*	Ref: Knuth Algorithm X Vol 2 p 342
		ignoring u2, v2, t2
		and avoiding negative numbers.
		return non-zero if inverse undefined.
	*/
	DIGIT_T u1[MAX_DIG_LEN], u3[MAX_DIG_LEN], v1[MAX_DIG_LEN], v3[MAX_DIG_LEN];
	DIGIT_T t1[MAX_DIG_LEN], t3[MAX_DIG_LEN], q[MAX_DIG_LEN];
	DIGIT_T w[2*MAX_DIG_LEN];

	int bIterations;
	int result;

	/* Step X1. Initialise */
	big_set_digit(u1, 1, ndigits);		/* u1 = 1 */
	big_set_equal(u3, u, ndigits);		/* u3 = u */
	big_set_zero(v1, ndigits);			/* v1 = 0 */
	big_set_equal(v3, v, ndigits);		/* v3 = v */

	bIterations = 1;	/* Remember odd/even iterations */
	while (!big_is_zero(v3, ndigits))		/* Step X2. Loop while v3 != 0 */
	{					/* Step X3. Divide and "Subtract" */
		big_divide(q, t3, u3, ndigits, v3, ndigits);
						/* q = u3 / v3, t3 = u3 % v3 */
		big_multiply(w, q, v1, ndigits);	/* w = q * v1 */
		big_add(t1, u1, w, ndigits);		/* t1 = u1 + w */

		/* Swap u1 = v1; v1 = t1; u3 = v3; v3 = t3 */
		big_set_equal(u1, v1, ndigits);
		big_set_equal(v1, t1, ndigits);
		big_set_equal(u3, v3, ndigits);
		big_set_equal(v3, t3, ndigits);

		bIterations = -bIterations;
	}

	if (bIterations < 0)
		big_sub(inv, v, u1, ndigits);	/* inv = v - u1 */
	else
		big_set_equal(inv, u1, ndigits);	/* inv = u1 */

	/* Make sure u3 = gcd(u,v) == 1 */
	if (short_cmp(u3, 1, ndigits) != 0)
	{
		result = 1;
		big_set_zero(inv, ndigits);
	}
	else
		result = 0;

	/* Clear up */
	big_set_zero(u1, ndigits);
	big_set_zero(v1, ndigits);
	big_set_zero(t1, ndigits);
	big_set_zero(u3, ndigits);
	big_set_zero(v3, ndigits);
	big_set_zero(t3, ndigits);
	big_set_zero(q, ndigits);
	big_set_zero(w, 2*ndigits);

	return result;
}

int FileWriter::big_is_zero(const DIGIT_T a[], unsigned long ndigits)
{
	/*	Returns true if a == 0, else false
	*/

	unsigned long  i;
	if (ndigits == 0) return -1;

	for (i = 0; i < ndigits; i++)	/* Start at lsb */
	{
		if (a[i] != 0)
         return false;	/* False */
	}

   return true;
}

int FileWriter::short_cmp(const DIGIT_T a[], DIGIT_T b, unsigned long ndigits)
{
	/* Returns sign of (a - b) where b is a single digit */

	size_t i;

	/* Changed in Ver 2.0 to cope with zero-length a */
	if (ndigits == 0) return (b ? -1 : 0);

	for (i = 1; i < ndigits; i++)
	{
		if (a[i] != 0)
			return 1;	/* GT */
	}

	if (a[0] < b)
		return -1;		/* LT */
	else if (a[0] > b)
		return 1;		/* GT */

	return 0;	/* EQ */
}

